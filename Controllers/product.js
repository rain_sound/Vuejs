'use strict'

const Product = require('../Models/product')

function getProduct(req, res){
  let productId = req.params.productId
  Product.findById(productId, (err, product) => {
    if(err) return res.status(500).send({message: 'Error al realizar la peticion'})
    if(!product) return res.status(404).send({message:'El producto no existe'})
    res.status(200).send({ product })
  })
}

function getProducts(req, res) {
  //modelo . buscar(todos los productos), error y data
  Product.find({}, (err, products) => {
    if(err) return res.status(500).send({message: 'Error al realizar la peticion'})
    if(!products) return res.status(404).send({message:'No existen productos para mostrar'})
    res.status(200).send({ products })
  })
}

function updateProduct(req, res){
  let productId = req.params.productId
  let update = req.body
  Product.findByIdAndUpdate(productId, update, (err, product) => {
      if(err) return res.status(500).send({message: 'Error al actualizar producto'})
      if(!product) res.status(404).send({message: 'Error producto no existe'})
      res.status(200).send({product})
    })
}

function deleteProduct(req, res){
  let productId = req.params.productId
  Product.findById(productId, (err, product) => {
    product.remove(err => {
      if(err) return res.status(500).send({message: 'Error al eliminar producto'})
      res.status(500).send('El producto ha sido eliminado satisfactoriamente')
    })
  })
}

function saveProduct(req, res){
  console.log('Post /api/product')
  console.log(req.body)
  let product = new Product()
  product.name = req.body.name
  product.picture = req.body.picture
  product.price = req.body.price
  product.category = req.body.category
  product.description = req.body.description
  //guardar en la base de datos
  product.save((err, productStored) => {
    if(err) res.status(500).send({message: 'Error al guardar el producto'})
    res.status(200).send({ product: productStored })
  })
}

module.exports = {
  getProduct,
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct
}
