'use strict'

const express = require('express')
const ProductCtrl = require('../Controllers/product')
const api = express.Router()
//obj.metodo(url, callback => {funcionalidad...})
//Controladores
api.get('/api/products', ProductCtrl.getProducts)
api.get('/api/product/:productId', ProductCtrl.getProduct)
api.post('/api/product', ProductCtrl.saveProduct)
api.put('/api/product/:productId', ProductCtrl.updateProduct)
api.delete('/api/product/:productId',ProductCtrl.deleteProduct)

module.exports = api
